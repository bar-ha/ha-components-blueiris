"""
Support for BlueIris.
For more details about this platform, please refer to the documentation at
https://home-assistant.io/components/switch.blueiris/
"""
import asyncio
import logging

from homeassistant.core import callback
from homeassistant.helpers.dispatcher import async_dispatcher_connect

from homeassistant.components.switch import SwitchDevice
from custom_components.blueiris import (SIGNAL_UPDATE_BLUEIRIS, DATA_BLUEIRIS, DOMAIN, CONF_PROFILE, ATTR_ADMIN_PROFILE, CONF_USERNAME, CONF_PASSWORD, CONF_PROFILE_ARMED, CONF_PROFILE_UNARMED)

_LOGGER = logging.getLogger(__name__)

DEPENDENCIES = [DOMAIN]
DEFAULT_ICON = 'mdi:alarm-light'

@asyncio.coroutine
def async_setup_platform(hass, config, async_add_entities, discovery_info=None):
    """Set up the BlueIris switch platform."""
    biData = hass.data.get(DATA_BLUEIRIS)

    if not biData:
        return

    profile_switch = BlueIrisProfileSwitch(biData)

    async_add_entities([profile_switch], True)

class BlueIrisProfileSwitch(SwitchDevice):
    """An abstract class for an BlueIris arm switch."""

    def __init__(self, biData):
        """Initialize the settings switch."""
        super().__init__()

        self._biData = biData

        self._name = 'BI Alerts'
        self._state = False

    @property
    def name(self):
        """Return the name of the node."""
        return self._name

    async def async_added_to_hass(self):
        """Register callbacks.""" 
        async_dispatcher_connect(self.hass, SIGNAL_UPDATE_BLUEIRIS, self._update_callback)

    @callback
    def _update_callback(self):
        """Call update method."""
        self.async_schedule_update_ha_state(True)

    @asyncio.coroutine
    def async_update(self):
        """Get the updated status of the switch."""

        self._state = self._biData.isBlueIrisArmed()

    @property
    def is_on(self):
        """Return the boolean response if the node is on."""
        return self._state

    @asyncio.coroutine
    def async_turn_on(self, **kwargs):
        """Turn device on."""
        self._biData.updateBlueIrisProfile(True)
        self.async_schedule_update_ha_state()

    @asyncio.coroutine
    def async_turn_off(self, **kwargs):
        """Turn device off."""
        self._biData.updateBlueIrisProfile(False)        
        self.async_schedule_update_ha_state()

    @property
    def icon(self):
        """Return the icon for the switch."""
        return DEFAULT_ICON


"""
Support for BlueIris.
For more details about this platform, please refer to the documentation at
https://home-assistant.io/components/camera.blueiris/
"""
import asyncio
import logging
from contextlib import closing

import aiohttp
import async_timeout
import requests

from homeassistant.core import callback
from homeassistant.const import (CONF_NAME)
from homeassistant.components.camera import (Camera)
from homeassistant.helpers.aiohttp_client import (async_get_clientsession, async_aiohttp_proxy_web)
from homeassistant.const import CONF_NAME
from custom_components.blueiris import (CONF_CAMERAS, SIGNAL_UPDATE_BLUEIRIS, DOMAIN, DATA_BLUEIRIS, ATTR_IMAGE_URL, ATTR_MJPEG_URL)
from homeassistant.helpers.dispatcher import async_dispatcher_connect

DEPENDENCIES = [DOMAIN]

_LOGGER = logging.getLogger(__name__)

@asyncio.coroutine
def async_setup_platform(hass, config, async_add_entities, discovery_info=None):
    """Set up a BlueIris Camera."""
    biData = hass.data.get(DATA_BLUEIRIS)

    if not biData:
        return

    configuration = biData.getAllCameras()

    print(configuration)

    cameras = configuration[CONF_CAMERAS]
        
    bi_camera_list = []
    for camera_id in cameras:
        camera = cameras[camera_id]
        _LOGGER.info('Processing new camera: {}'.format(camera))
        
        name = camera[CONF_NAME]  
        mjpeg_url = camera[ATTR_MJPEG_URL]
        image_url = camera[ATTR_IMAGE_URL]

        bi_camera = BlueIrisCamera(name, image_url, mjpeg_url)
        bi_camera_list.append(bi_camera)

        _LOGGER.info('Camera created: {}'.format(bi_camera))
    
    async_add_entities(bi_camera_list)


def extract_image_from_mjpeg(stream):
    """Take in a MJPEG stream object, return the jpg from it."""
    data = b''
    for chunk in stream:
        data += chunk
        jpg_start = data.find(b'\xff\xd8')
        jpg_end = data.find(b'\xff\xd9')
        if jpg_start != -1 and jpg_end != -1:
            jpg = data[jpg_start:jpg_end + 2]
            return jpg


class BlueIrisCamera(Camera):
    """An implementation of an IP camera that is reachable over a URL."""

    def __init__(self, name, image_url, mjpeg_url):
        """Initialize a BlueIris camera."""
        super().__init__()
        self._name = 'BI {}'.format(name)
        self._mjpeg_url = mjpeg_url
        self._still_image_url = image_url

        
    @asyncio.coroutine
    def async_camera_image(self):
        """Return a still image response from the camera."""
        websession = async_get_clientsession(self.hass)
        try:
            with async_timeout.timeout(10, loop=self.hass.loop):
                response = yield from websession.get(self._still_image_url)

                image = yield from response.read()
                return image

        except asyncio.TimeoutError:
            _LOGGER.error("Timeout getting camera image")

        except aiohttp.ClientError as err:
            _LOGGER.error("Error getting new camera image: %s", err)

    def camera_image(self):
        """Return a still image response from the camera."""
        req = requests.get(self._mjpeg_url, stream=True, timeout=10)

        # https://github.com/PyCQA/pylint/issues/1437
        # pylint: disable=no-member
        with closing(req) as response:
            return extract_image_from_mjpeg(response.iter_content(102400))

    async def handle_async_mjpeg_stream(self, request):
        """Generate an HTTP MJPEG stream from the camera."""
        # connect to stream
        websession = async_get_clientsession(self.hass)
        stream_coro = websession.get(self._mjpeg_url)

        return await async_aiohttp_proxy_web(self.hass, request, stream_coro)

    @property
    def name(self):
        """Return the name of this camera."""
        return self._name

    async def async_added_to_hass(self):
        """Register callbacks."""
        async_dispatcher_connect(
            self.hass, SIGNAL_UPDATE_BLUEIRIS, self._update_callback)

    @callback
    def _update_callback(self):
        """Call update method."""
        self.async_schedule_update_ha_state()
    
    def enable_motion_detection(self):
        """Enable the Motion detection in base station (Arm)."""
        self._motion_status = True

    def disable_motion_detection(self):
        """Disable the motion detection in base station (Disarm)."""
        self._motion_status = False